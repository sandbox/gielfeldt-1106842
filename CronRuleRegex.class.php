<?php
// $Id$

/**
 * @file
 *
 * by Thomas Gielfeldt
 * <thomas@gielfeldt.com>
 *
 * This class parses cron rules and determines last execution time using regular expressions.
 *
 */

class CronRuleRegex {

  public $rule = NULL;

  /**
   * Constructor
   */
  function __construct($rule) {
    $this->rule = $rule;
  }

  /**
   * Expand interval from cronrule part
   *
   * @param $matches (e.g. 4-43/5)
   *   array of matches:
   *     [1] = lower
   *     [2] = upper
   *     [4] = step
   *
   * @return
   *   (string) comma-separated list of values
   */
  function expandInterval($matches) {
    $result = array();
    $step = ($matches[4] > 0) ? $matches[4] : 1;
    for ($i = $matches[1]; $i <= $matches[2]; $i+=$step) {
      $result[] = $i % ($matches[2] + 1);
    }
    return implode(',', $result);
  }

  /**
   * Expand range from cronrule part
   *
   * @param $rule
   *   (string) cronrule part, e.g.: 1,2,3,4-43/5
   * @param $max
   *   (string) boundaries, e.g.: 0-59
   * @param $digits
   *   (int) number of digits of value (leading zeroes)
   * @return
   *   (array) array of valid values
   */
  function expandRange($rule, $max, $digits = 2) {
    $rule = str_replace("*", $max, $rule);
    $rule = preg_replace_callback('/(\d+)-(\d+)(\/(\d+))?/', array($this, 'expandInterval'), $rule);
    $rule = explode(',', $rule);
    foreach ($rule as &$r) $r = sprintf("%0{$digits}d", $r);
    return $rule;
  }

  /**
   * Generate regex rules
   *
   * @param $rule
   *   (string) cronrule, e.g: 1,2,3,4-43/5 * * * 2,5
   * @return
   *   (array) date and time regular expression for mathing rule
   */
  function generateRegExps($rule) {
    list($minutes, $hours, $days, $months, $weekdays) = preg_split('/\s+/', $this->rule);
    $regex = array();
    $regex[] = '(' . implode('|', $this->expandRange($minutes, '0-59')) . ')';
    $regex[] = '(' . implode('|', $this->expandRange($hours, '0-23')) . ')';
    $regex[] = '(' . implode('|', $this->expandRange($days, '1-31')) . ')';
    $regex[] = '(' . implode('|', $this->expandRange($months, '1-12')) . ')';
    $regex[] = '(' . implode('|', $this->expandRange($weekdays, '0-6', 1)) . ')';

    return array(
      'date'   => implode('\\s+', array($regex[2], $regex[3], $regex[4])),
      'time'   => implode('\\s+', array($regex[0], $regex[1])),
    );
  }

  /**
   * Get last execution time of rule in unix timestamp format
   *
   * @param $time
   *   (int) time to use as relative time (default now)
   * @return
   *   (int) unix timestamp of last execution time
   */
  function getLastRan($time = NULL) {
    // Current time round to last minute
    if (!isset($time)) $time = time();
    $time = strtotime(gmdate('Y-m-d H:i:00 +0000', $time));

    // Generate regular expressions from rule
    $regexps = $this->generateRegExps($this->rule);

    // Max iterations
    $i = 365 * 28; // Go back max 28 years (leapyear * weekdays)
    $boundary = FALSE;

    $regexp = $regexps['date'];
    do {
      $now = date('d m w', $time);
      if (preg_match("/^\s*$regexp\s*$/", $now)) {
        break;
      }
      $time -= 86400;
      $boundary = TRUE;
    } while ($i--);

    if (!$i) {
      // WARNING: Max iteration reached ... shouldn't happen
    }

    // If time has been modified we must start from 23:59 (last minute on the day)
    if ($boundary) $time = strtotime(date('Y', $time) . '-' . date('m', $time) . '-' . date('d', $time) . ' 23:59:00');

    // Max iterations
    $i = 1440;

    $regexp = $regexps['time'];
    do {
      $now = date('i H', $time);
      if (preg_match("/^\s*$regexp\s*$/", $now)) {
        break;
      }
      $time -= 60;
    } while ($i--);

    if (!$i) {
      // WARNING: Max iteration reached ... shouldn't happen
    }

    return $time;
  }
}
